from django.db import models
from django.urls import reverse


class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    bin_number = models.CharField(max_length=200, unique=True)


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=100)
    name = models.CharField(max_length=50)
    color = models.CharField(max_length=40)
    picture_url = models.URLField(null=True)

    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("show_shoe", kwargs={"pk": self.pk})
