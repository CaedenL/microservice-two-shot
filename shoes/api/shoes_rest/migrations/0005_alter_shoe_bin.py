# Generated by Django 4.0.3 on 2023-04-20 13:39

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0004_rename_model_name_shoe_name_remove_shoe_bin_number_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shoe',
            name='bin',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='shoes', to='shoes_rest.binvo'),
        ),
    ]
