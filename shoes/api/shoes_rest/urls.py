from django.urls import path
from .views import list_shoes, show_shoe


urlpatterns = [
    path("shoes/", list_shoes, name="api_create_shoes"),
    path("bins/<int:bin_vo_id>/shoes/", list_shoes, name="api_list_shoes"),
    path("shoes/<int:pk>/", show_shoe, name="api_show_shoe"),
]
