import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);


async function loadShoes() {
  const response = await fetch('http://localhost:8080/api/shoes/');
  console.log(response);
  if (response.ok) {
    const data = await response.json();
    console.log(data);
    root.render(
      <React.StrictMode>
        <App shoes={data.shoes} />
      </React.StrictMode>
    )
  } else {
    console.error(response);
  }
}
loadShoes();
async function loadItems() {
  const response1 = await fetch('http://localhost:8090/api/hats/');

  const data1 = await response1.json();

  root.render(
    <React.StrictMode>
      <App hats={data1.hats} />
  </React.StrictMode>
  )
}

loadItems();
