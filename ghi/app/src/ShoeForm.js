import React, { useEffect, useState } from 'react';

function ShoeForm () {
    const [bins, setBins] = useState([]);

    const [manufacturer, setManufacturer] = useState('');
    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const [color, setColor] = useState('');
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const [picture_url, setPicture] = useState('');
    const handlePictureChange = (event) => {
        const value = event.target.value;
        setPicture(value);
    }

    const [bin, setBin] = useState('');
    const handleBinChange = (event) => {
        const value = event.target.value;
        setBin(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.manufacturer = manufacturer;
        data.name = name;
        data.color = color;
        data.picture_url = picture_url;
        data.bin = bin;
        console.log(data);

        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe)

            setManufacturer('');
            setName('');
            setColor('');
            setPicture('');
            setBin('');
        }

    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);

            // const selectTag = document.getElementById('bin');
            // for (let bin of data.bins) {
            //   const option = document.createElement('option');
            //   option.value = bin.bin_number;
            //   option.innerHTML = bin.bin_number;
            //   selectTag.appendChild(option);
            // }
        }
    }

    useEffect(() => {
        fetchData();
      }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Add a new Shoe</h1>
            <form onSubmit={handleSubmit} id="add-shoe-form">
                <div className="form-floating mb-3">
                    <input value={manufacturer} onChange={handleManufacturerChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                    <label htmlFor="manufacturer">Manufacturer</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                    <label htmlFor="name">Model Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={color} onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                    <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={picture_url} onChange={handlePictureChange} placeholder="Picture" type="text" name="picture_url" id="picture_url" className="form-control"/>
                    <label htmlFor="picture">Picture URL</label>
                </div>
                <div className="mb-3">
                    <select onChange={handleBinChange} required name="bin" id="bin" className="form-select">
                    <option value="">Choose a Bin</option>
                        {bins.map(bin => {
                            return (
                            <option key={bin.bin_number} value={bin.bin_number}>
                                {bin.bin_number}
                            </option>
                            );
                          })}
                        </select>
                </div>
                <button className="btn btn-primary">Add</button>
            </form>
            </div>
        </div>
        </div>
    );
}

export default ShoeForm
