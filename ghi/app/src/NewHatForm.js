import React, { useEffect, useState} from 'react';
import { useNavigate } from 'react-router-dom';


function HatForm(){
    const navigate = useNavigate();

    const [locations, selectLocation] = useState([]);
    const [style, setStyle] = useState('');
    const [color, setColor] = useState('');
    const [fabric, setFabric] = useState('');
    const [picture, setPicture] = useState('');
    const [location, setLocation] = useState('');

    const handleStyle = (event) => {
        const value = event.target.value;
        setStyle(value);
    }

    const handleColor = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handleFabric = (event) => {
        const value = event.target.value;
        setFabric(value);
    }

    const handlePicture = (event) => {
        const value = event.target.value;
        setPicture(value);
    }

    const handleLocation = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.style = style;
        data.color = color;
        data.fabric = fabric;
        data.picture = picture;
        data.location = location;

        const hatUrl = "http://localhost:8090/api/hats/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat);

            navigate('/hats')
        }
    }

    const fetchData = async () => {
        const url = "http://localhost:8100/api/locations/";

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            selectLocation(data.locations);
        }
    };

    useEffect(() =>{
        fetchData();
    }, []);


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a new hat!</h1>
                    <form onSubmit={handleSubmit} id="create-hat-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleStyle} value={style} placeholder="Style" required type="text" id="style" className="form-control" name="style" />
                            <label htmlFor="style">Style</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleColor} value={color} placeholder="Color" required type="text" id="color" className="form-control" name="color" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFabric} value={fabric} placeholder="Fabric" required type="text" id="fabric" className="form-control" name="fabric" />
                            <label htmlFor="fabric">Fabric</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePicture} value={picture} placeholder="Picture" required type="text" id="picture" className="form-control" name="picture" />
                            <label htmlFor="picture">Picture url</label>
                        </div>
                        <div className="mb-3">
                            <select required onChange={handleLocation} value={location} id="location" className="form-select" name="location">
                                <option value="">Choose a location</option>
                                {locations.map(location => {
                                    return (
                                        <option key={location.href} value={location.href}>{location.closet_name}: Section {location.section_number}/Shelf {location.shelf_number}</option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default HatForm;
