import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatList from './HatList';
import HatForm from './NewHatForm';


function App(props) {
  if (props.hats === undefined) {
    return null;
  }

import ShoesList from './ShoesList';
import ShoeForm from './ShoeForm';

function App(props) {
  if (props.shoes === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes">
            <Route path="" element={<ShoesList shoes={ props.shoes } />} />
          </Route>
          <Route path="shoes">
            <Route path="new" element={<ShoeForm />} />
          <Route path="hats">
            <Route path="/hats" element={<HatList hats={props.hats} />} />
            <Route path="new" element={<HatForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
