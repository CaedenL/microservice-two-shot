function ShoesList(props) {
    if (props.shoes === undefined) {
        return null;
    }
    return (
        <table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Bin</th>
                </tr>
            </thead>
            <tbody>
                {props.shoes.map(shoe => {
                    return (
                        <tr key={shoe.id}>
                            <td>{ shoe.name }</td>
                            <td>{ shoe.bin.bin_number }</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    )
}

export default ShoesList;
