import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';


function HatColumn(props){

    return (
      <div className="col">
        {props.hats.map(hat => {
          return (
            <div key={hat.id} className="card h-50 mb-3 shadow">
              <img src={hat.picture} className="card-img-top" />
              <div className="card-body">
                <h5 className="card-title">{hat.style}</h5>
                <ul className="list-group list-group-flush">
                  <li className="list-group-item">{hat.color}</li>
                  <li className="list-group-item">{hat.fabric}</li>
                  <li className="list-group-item">{hat.location.closet_name}: Section {hat.location.section_number} / Shelf {hat.location.shelf_number}</li>
                </ul>
              </div>
              <div className="card-footer text-muted">
                <button onClick={() => {
                    const hatUrl = `http://localhost:8090/api/hats/${hat.id}/`
                    fetch(hatUrl, { method: "DELETE"}).then((response) => {
                      if(!response.ok){
                        throw new Error('Something went wrong');
                      }
                      window.location.reload(false);
                    })
                    .catch((e) => {
                        console.log(e);
                    });
                }} className="btn btn-danger">Delete</button>
              </div>
            </div>
          );
        })}
      </div>
    );
  }

const HatList = (props) => {
  const [hatColumns, setHatColumns] = useState([[], [], []])

  const fetchData = async () => {
    const url = 'http://localhost:8090/api/hats/';

    try {
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        const requests = [];
        for (let hat of data.hats) {
          const detailUrl = `http://localhost:8090/api/hats/${hat.id}`;
          requests.push(fetch(detailUrl));
        }

        const responses = await Promise.all(requests);

        const columns = [[], [], []];

        let i = 0;
          for (const hatResponse of responses) {
            if (hatResponse.ok) {
              const details = await hatResponse.json();
              columns[i].push(details);
              i = i +1;
              if (i > 2) {
                i = 0;
              }
          } else {
            console.error(hatResponse);
          }
        }

        setHatColumns(columns);
      }
    } catch (e) {
      console.error(e);
      }
    }

    useEffect(() => {
      fetchData();
    }, []);



    return (
      <>
        <div className="container">
            <h2 className="text-center pt-3">Your Hats</h2>
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center p-3">
              <Link to="/hats/new" className="btn btn-primary btn-lg px-4 gap-3">Add a hat</Link>
            </div>
            <div className="row">
                {hatColumns.map((hatList, id) => {
                    return (
                        <HatColumn key={id} hats={hatList} />
                    );
                })}
            </div>
        </div>
      </>
    );
}

export default HatList;
