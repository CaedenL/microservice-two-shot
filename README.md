# Wardrobify

Team:

* Caeden Lathem - Hats
* Cape Monn - Shoes

## Design

* React design, hats are loaded into bootstrap cards on hats page of the application.

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice

* Hat models include style, fabric, color, and location.
* Hats are identifed by their unique id.
* The cards have a delete function to remove the hat from the database.
* A create button is on the hat list page.
* New hat form allows for the creation of a new hat.
