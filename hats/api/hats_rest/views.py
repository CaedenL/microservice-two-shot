from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from django.http import JsonResponse
from hats_rest.models import Hat, LocationVO
import json


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name",
        "section_number",
        "shelf_number",
        ]


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "style",
        "color",
        "fabric",
        "picture",
        ]

    encoders = {
        "locations": LocationVODetailEncoder()
    }

    def get_extra_data(self, o):
        return {
                "import_href": o.location.import_href,
                "closet_name": o.location.closet_name,
                "section_number": o.location.section_number,
                "shelf_number": o.location.shelf_number
            }


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "style",
        "fabric",
        "color",
        "picture",
        "location",
        ]
    encoders = {
        "location": LocationVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hat.objects.filter(location=location_vo_id)
        else:
            hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Location does not exist"},
                status=400,
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_hat(request, id):
    if request.method == "GET":
        hat = Hat.objects.get(id=id)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "location" in content:
                location_href = content["location"]
                location = LocationVO.objects.get(import_href=location_href)
                content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Location does not exist"},
                status=400,
            )
        Hat.objects.filter(id=id).update(**content)
        hat = Hat.objects.get(id=id)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
