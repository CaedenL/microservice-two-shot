# Generated by Django 4.0.3 on 2023-04-19 20:00

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0004_remove_hatvo_bin_number'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='HatVO',
            new_name='LocationVO',
        ),
    ]
